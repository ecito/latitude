//
//  PlacesViewController.m
//  Latitude
//
//  Created by Andre Navarro on 11/1/11.
//  Copyright (c) 2011 Disruptive Media Labs, LLC. All rights reserved.
//

#import "PlacesViewController.h"
#import "Constants.h"
#import "AFNetworking.h"
#import "Location.h"
#import "PlaceTableViewCell.h"
#import "TTTLocationFormatter.h"
#import "Place.h"
#import "RedMeasleAnnotationView.h"
#import "GTMHTTPFetcher.h"
#import "SVProgressHUD.h"
#import "GTMOAuth2Authentication.h"

@interface PlacesViewController()

- (void)deleteLocation;
- (NSMutableArray*)sortedPlaces;
- (void)zoomToFitAnnotations;

@end

@implementation PlacesViewController
@synthesize location;
@synthesize places;
@synthesize mapView;
@synthesize tableView;
@synthesize auth;

- (void)dealloc {
  self.location = nil;
  self.places = nil;
  self.mapView = nil;
  self.tableView = nil;
  self.auth = nil;
  
  [super dealloc];
}


#pragma mark - View lifecycle

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
  return UIInterfaceOrientationIsPortrait(interfaceOrientation) || UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)viewDidLoad {
  [super viewDidLoad];
  
  self.title = @"Nearby places";
  
  self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Delete"
                                                                             style:UIBarButtonItemStyleBordered
                                                                            target:self
                                                                            action:@selector(deleteLocationButtonTapped)] autorelease];
  self.navigationItem.rightBarButtonItem.tintColor = [UIColor redColor];
  
  self.tableView.rowHeight = 70.0f;
}

- (void)deleteLocationButtonTapped {
  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete" 
                                                  message:@"Are you sure you want to delete this location from your Google Latitude history?" 
                                                 delegate:self
                                        cancelButtonTitle:@"Cancel"
                                        otherButtonTitles:@"Yes", nil];
  [alert show];
  [alert release];  
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
  if (buttonIndex == 1) {
    [self deleteLocation];
  }
}

- (void)deleteLocation {


  NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.googleapis.com/latitude/v1/location/%.0f", self.location.timestamp]];
  NSLog(@"URL: %@", url);
  
  [SVProgressHUD showInView:self.view withMaskType:SVProgressHUDMaskTypeGradient];
  
  NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
  request.HTTPMethod = @"DELETE";
  
  [self.auth authorizeRequest:request
            completionHandler:^(NSError *error) {
              if (error) {
                
                [SVProgressHUD dismissWithError:@"Error"];
              } else {
                
                GTMHTTPFetcher* fetcher = [GTMHTTPFetcher fetcherWithRequest:request];
                
                [fetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *fetchError) {
                  
                  if (fetchError != nil) {
                    [SVProgressHUD dismissWithError:@"Error"];
                  } else {
                    
                    [SVProgressHUD dismissWithSuccess:@"Deleted"];

                    [self performSelector:@selector(closeView) withObject:nil afterDelay:1.0];
                  }
                }];
                
              }
              
            }];
}

- (void)closeView {
  [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
  
  // add Location pin and circle
  [self.mapView addAnnotation:self.location];
  [self.mapView selectAnnotation:self.location animated:YES];
  
  MKCircle *accuracyCircle = [MKCircle circleWithCenterCoordinate:self.location.coordinate radius:self.location.accuracy];
  [self.mapView addOverlay:accuracyCircle];
  
  // request places
  NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/search/json?types=establishment&location=%f,%f&radius=%.0f&sensor=false&key=%@", 
                                     self.location.coordinate.latitude,
                                     self.location.coordinate.longitude,
                                     self.location.accuracy,
                                     kGoogleSimpleAPIKey]];
  NSLog(@"URL: %@", url);
  
  NSURLRequest *request = [NSURLRequest requestWithURL:url];
  AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {

    self.places = [NSMutableArray array];
    for (NSDictionary *plc in [JSON valueForKeyPath:@"results"]) {
      [self.places addObject:[Place placeFromJSONDictionary:plc]];
    }
    
    self.places = [self sortedPlaces];
    
    [self.tableView reloadData];
    [self.mapView addAnnotations:self.places];
    [self zoomToFitAnnotations];
    
  } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" 
                                                    message:@"Could not load places nearby" 
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];  
  }];

  
  NSOperationQueue *queue = [[[NSOperationQueue alloc] init] autorelease];
  [queue addOperation:operation];
}

- (NSMutableArray*)sortedPlaces {
  return [NSMutableArray arrayWithArray:[self.places sortedArrayUsingComparator:^ NSComparisonResult(id obj1, id obj2) {
    CLLocationDistance d1 = [[(Place *)obj1 location] distanceFromLocation:self.location.location];
    CLLocationDistance d2 = [[(Place *)obj2 location] distanceFromLocation:self.location.location];
    
    if (d1 < d2) {
      return NSOrderedAscending;
    } else if (d1 > d2) {
      return NSOrderedDescending;
    } else {
      return NSOrderedSame;
    }
  }]];  
}
#pragma mark - MKMapViewDelegate

- (void)zoomToFitAnnotations {
  if ([[self.mapView annotations] count] == 0) {
    return;
  }
  
  int i = 0;
  MKMapPoint points[[self.mapView.annotations count]];
  
  //build array of annotation points
  for (id<MKAnnotation> annotation in [self.mapView annotations])
    points[i++] = MKMapPointForCoordinate(annotation.coordinate);
  
  MKPolygon *poly = [MKPolygon polygonWithPoints:points count:i];
  
  [self.mapView setRegion:MKCoordinateRegionForMapRect([poly boundingMapRect]) animated:YES]; 
}

#pragma mark - MKMapViewDelegate

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay {
  if ([overlay isKindOfClass:[MKCircle class]]) {
    MKCircleView *circleView = [[[MKCircleView alloc] initWithCircle:overlay] autorelease];
    circleView.lineWidth = 1.0;
    circleView.strokeColor = [UIColor blueColor];
    circleView.fillColor = [UIColor colorWithRed:0.0 green:0.0 blue:1.0 alpha:0.2];
    
    return circleView;
  }
 
  return nil;
}

- (MKAnnotationView *)mapView:(MKMapView *)theMapView viewForAnnotation:(id <MKAnnotation>)annotation {
  // if it's the user location, just return nil.
  if ([annotation isKindOfClass:[MKUserLocation class]])
    return nil;
  
  if ([annotation isKindOfClass:[Place class]]) {
    static NSString* annotationIdentifier = @"PlaceAnnotationIdentifier";
    MKPinAnnotationView* pinView = (MKPinAnnotationView *)
    [theMapView dequeueReusableAnnotationViewWithIdentifier:annotationIdentifier];
    if (!pinView) {
      pinView = [[[MKPinAnnotationView alloc]
                  initWithAnnotation:annotation reuseIdentifier:annotationIdentifier] autorelease];
      pinView.canShowCallout = YES;  
      
    }
    else  {
      pinView.annotation = annotation;
    }
    
    return pinView;

  }
  else if ([annotation isKindOfClass:[Location class]]) {
    static NSString* annotationIdentifier = @"LocationAnnotationIdentifier";
    RedMeasleAnnotationView* pinView = (RedMeasleAnnotationView *)
    [theMapView dequeueReusableAnnotationViewWithIdentifier:annotationIdentifier];
    if (!pinView) {
      pinView = [[[RedMeasleAnnotationView alloc]
                  initWithAnnotation:annotation reuseIdentifier:annotationIdentifier] autorelease];
      pinView.canShowCallout = YES;  
      
    }
    else  {
      pinView.annotation = annotation;
    }
    
    return pinView;

  }
  return nil;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return [self.places count];
}

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  static NSString *CellIdentifier = @"Cell";
  
  PlaceTableViewCell *cell = (PlaceTableViewCell *)[aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
  if (cell == nil) {
    cell = [[[PlaceTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
  }
  
  Place *place = [self.places objectAtIndex:indexPath.row];
  
  static TTTLocationFormatter *_locationFormatter = nil;
  if (!_locationFormatter) {
    _locationFormatter = [[TTTLocationFormatter alloc] init];
    if (![[[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem] boolValue]) {
      [_locationFormatter setUnitSystem:TTTImperialSystem]; 
    }
  }
  
  cell.detailTextLabel.text = [_locationFormatter stringFromDistanceAndBearingFromLocation:self.location.location toLocation:place.location];
  
  cell.place = place;
  
  return cell;
}

#pragma mark - UITableViewDelegate

- (NSString *)tableView:(UITableView *)aTableView titleForHeaderInSection:(NSInteger)section {
  if ([self tableView:aTableView numberOfRowsInSection:section] > 0) {
    return NSLocalizedString(@"Nearby Places", nil);
  }
  
  return nil;
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  [self.mapView selectAnnotation:[self.places objectAtIndex:indexPath.row] animated:YES];
  [aTableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
