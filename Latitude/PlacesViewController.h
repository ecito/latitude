//
//  PlacesViewController.h
//  Latitude
//
//  Created by Andre Navarro on 11/1/11.
//  Copyright (c) 2011 Disruptive Media Labs, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@class Location;
@class GTMOAuth2Authentication;

@interface PlacesViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate> {

}

@property (nonatomic, retain) Location *location;
@property (nonatomic, retain) NSMutableArray *places;
@property (retain, nonatomic) IBOutlet MKMapView *mapView;
@property (retain, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, retain) GTMOAuth2Authentication *auth;

@end
