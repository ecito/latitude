//
//  ViewController.h
//  Latitude
//
//  Created by Andre Navarro on 10/30/11.
//  Copyright (c) 2011 Disruptive Media Labs, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GTMOAuth2Authentication;

@protocol AccountViewControllerDelegate <NSObject>

- (void)didSignInWithAuth:(GTMOAuth2Authentication*)auth;
- (void)didSignOut;
- (void)didCancelAccountView;

@end

@interface AccountViewController : UIViewController

@property (retain, nonatomic) IBOutlet UIButton *signInOutButton;
@property (nonatomic, assign) id<AccountViewControllerDelegate> delegate;

- (IBAction)signInOut:(id)sender;

@end
