//
//  MapViewController.h
//  Latitude
//
//  Created by Andre Navarro on 10/30/11.
//  Copyright (c) 2011 Disruptive Media Labs, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@class GTMOAuth2Authentication;
@class CrumbPath;
@class CrumbPathView;
@class DateNavigationView;
@protocol AccountViewControllerDelegate;
@protocol DateNavigationViewDelegate;
@protocol DateSelectionViewControllerDelegate;

@interface MapViewController : UIViewController <MKMapViewDelegate,
AccountViewControllerDelegate,
DateNavigationViewDelegate,
DateSelectionViewControllerDelegate,
UIPopoverControllerDelegate> {
  
  MKMapView *mapView;
  
  MKCircle *accuracyCircle;
  NSDate *currentDate;

  CrumbPath *crumbs;
  CrumbPathView *crumbView;
  
  GTMOAuth2Authentication *auth;  
}

@property (nonatomic, retain) NSDate *currentDate;
@property (nonatomic, retain) GTMOAuth2Authentication *auth;

@end
