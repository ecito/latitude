//
//  DateNavigationView.m
//  Latitude
//
//  Created by Andre Navarro on 10/31/11.
//  Copyright (c) 2011 Disruptive Media Labs, LLC. All rights reserved.
//

#import "DateNavigationView.h"

@implementation DateNavigationView
@synthesize dateSelectionItem;
@synthesize delegate;

- (id)initWithFrame:(CGRect)aFrame {
  NSArray * nib = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
  self = [[nib objectAtIndex:0] retain]; 
  if (self) {
    self.frame = aFrame;
    
  }
  return self;
}

- (IBAction)dateSelectionButtonTapped:(id)sender {
  if (self.delegate && [self.delegate respondsToSelector:@selector(dateSelectionButtonTapped:)]) {
    [self.delegate dateSelectionButtonTapped:self.dateSelectionItem];
  }
}

- (IBAction)previousDayButtonTapped:(id)sender {
  if (self.delegate && [self.delegate respondsToSelector:@selector(previousDayButtonTapped)]) {
    [self.delegate previousDayButtonTapped];
  }
}

- (IBAction)nextDayButtonTapped:(id)sender {
  if (self.delegate && [self.delegate respondsToSelector:@selector(nextDayButtonTapped)]) {
    [self.delegate nextDayButtonTapped];
  }
}

- (void)dealloc {
  self.dateSelectionItem = nil;
  
  [super dealloc];
}

@end
