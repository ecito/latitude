//
//  DateSelectionViewController.m
//  Latitude
//
//  Created by Andre Navarro on 10/31/11.
//  Copyright (c) 2011 Disruptive Media Labs, LLC. All rights reserved.
//

#import "DateSelectionViewController.h"

@implementation DateSelectionViewController
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
      
      self.title = @"Select a date";
      self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Cancel" 
                                                                                style:UIBarButtonItemStyleBordered
                                                                               target:self 
                                                                               action:@selector(cancel)]
                                               autorelease];
    }
    return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];

  self.view.backgroundColor = [UIColor darkGrayColor];
  
  TKCalendarMonthView *monthView = [[TKCalendarMonthView alloc] initWithSundayAsFirst:YES];
  [monthView selectDate:self.delegate.currentDate];
  monthView.delegate = self;
  monthView.dataSource = self;

  [self.view addSubview:monthView];
  [monthView release];
}

- (void)cancel {
  if (self.delegate && [self.delegate respondsToSelector:@selector(didCancelDateSelection)]) {
    [self.delegate didCancelDateSelection];
  }
}

#pragma mark - TKCalendarMonthViewDelegate

- (NSArray*) calendarMonthView:(TKCalendarMonthView*)monthView marksFromDate:(NSDate*)startDate toDate:(NSDate*)lastDate {
  return [NSArray array];
}

- (void) calendarMonthView:(TKCalendarMonthView*)monthView didSelectDate:(NSDate*)date {
  TKDateInformation info = [date dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	NSDate *myTimeZoneDay = [NSDate dateFromDateInformation:info timeZone:[NSTimeZone systemTimeZone]];
  
  if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectDate:)]) {
    [self.delegate didSelectDate:myTimeZoneDay];
  }
}

@end
