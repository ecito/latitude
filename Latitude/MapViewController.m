//
//  MapViewController.m
//  Latitude
//
//  Created by Andre Navarro on 10/30/11.
//  Copyright (c) 2011 Disruptive Media Labs, LLC. All rights reserved.
//

#import "MapViewController.h"
#import "Location.h"
#import "CrumbPath.h"
#import "CrumbPathView.h"
#import "AccountViewController.h"
#import "GTMOAuth2Authentication.h"
#import "GTMOAuth2ViewControllerTouch.h"
#import "Constants.h"
#import "SBJSON.h"
#import "DateNavigationView.h"
#import "NSDate+HumanInterval.h"
#import "DateSelectionViewController.h"
#import "SVProgressHUD.h"
#import "RedMeasleAnnotationView.h"
#import "PlacesViewController.h"

@interface MapViewController()

- (void)checkLogin;
- (void)presentAccountViewController;
- (void)getHistoryForSelectedDate;
- (void)updateMap;
- (void)addLines;
- (void)zoomToFitAnnotations;
- (void)changeCurrentDateByNumberOfDays:(NSInteger)days;
- (void)updateDateDisplay;

@property (nonatomic, retain) MKMapView *mapView;
@property (nonatomic, retain) NSMutableArray *locations;
@property (nonatomic, retain) MKCircle *accuracyCircle;
@property (nonatomic, retain) DateNavigationView *dateNavigationView;
@property (nonatomic, retain) CrumbPath *crumbs;
@property (nonatomic, retain) CrumbPathView *crumbView;

@property (nonatomic, retain) UIPopoverController *dateSelectionPopoverController;

@end


@implementation MapViewController

@synthesize auth;
@synthesize mapView;
@synthesize locations;
@synthesize currentDate;
@synthesize dateNavigationView;
@synthesize accuracyCircle;
@synthesize crumbs;
@synthesize crumbView;
@synthesize dateSelectionPopoverController;

- (void)dealloc {
  self.auth = nil;
  self.mapView = nil;
  self.locations = nil;
  self.currentDate = nil;
  self.dateNavigationView = nil;
  self.accuracyCircle = nil;
  self.crumbs = nil;
  self.crumbView = nil;
    self.dateSelectionPopoverController = nil;
  
  [super dealloc];
}

- (id)init {
  self = [super initWithNibName:nil bundle:nil];
  if (self) {
    
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Account" 
                                                                              style:UIBarButtonItemStyleBordered
                                                                             target:self 
                                                                             action:@selector(presentAccountViewController)] 
                                             autorelease];
  }
  return self;
}

#pragma mark - AccountViewControllerDelegate

- (void)didSignOut {
  self.auth = nil;
}

- (void)didSignInWithAuth:(GTMOAuth2Authentication *)anAuth {
  self.auth = anAuth;
  [self getHistoryForSelectedDate];

  [self dismissModalViewControllerAnimated:YES];
}

- (void)didCancelAccountView {
  [self dismissModalViewControllerAnimated:YES];  
}

#pragma mark - View lifecycle

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
  return UIInterfaceOrientationIsPortrait(interfaceOrientation) || UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
	if (self.dateSelectionPopoverController != nil) {
		[self.dateSelectionPopoverController dismissPopoverAnimated:YES];
	}
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
  [self zoomToFitAnnotations];
}

- (void)viewDidLoad {
  [super viewDidLoad];
    
  self.currentDate = [NSDate date];

  self.mapView = [[MKMapView alloc] initWithFrame:CGRectMake(self.view.bounds.origin.x,
                                                             self.view.bounds.origin.y,
                                                             self.view.bounds.size.width,
                                                             self.view.bounds.size.height - 44.0)];
  [self.mapView release];
  
  self.mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
  self.mapView.delegate = self;
  [self.view addSubview:self.mapView];
  
  self.dateNavigationView = [[DateNavigationView alloc] initWithFrame:CGRectMake(0.0,
                                                                                 self.view.frame.size.height - 44.0,
                                                                                 self.view.frame.size.width,
                                                                                 44.0)];
  [self.dateNavigationView release];
  
  self.dateNavigationView.delegate = self;
  [self.view addSubview:self.dateNavigationView];
  [self updateDateDisplay];

  UISegmentedControl *mapTypeSegControl = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"Road", @"Sat.", @"Hybrid", @"Terrain", nil]];
  mapTypeSegControl.selectedSegmentIndex = 0;
  mapTypeSegControl.segmentedControlStyle = UISegmentedControlStyleBar;
  self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:mapTypeSegControl] autorelease];;
  [mapTypeSegControl addTarget:self
                       action:@selector(changeMapType:)
             forControlEvents:UIControlEventValueChanged];
  [mapTypeSegControl release];  
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
  
  [self checkLogin];
}

- (void)checkLogin {
  
  self.auth = [GTMOAuth2ViewControllerTouch authForGoogleFromKeychainForName:kKeychainItemName
                                                                    clientID:kClientID
                                                                clientSecret:kClientSecret];
  
  if ([self.auth canAuthorize]) {
    [self getHistoryForSelectedDate];
  }
  else {
    [self presentAccountViewController];
  }
}

- (void)presentAccountViewController {
  AccountViewController *viewController = nil;
  if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
    viewController = [[[AccountViewController alloc] initWithNibName:@"AccountViewController_iPhone" bundle:nil] autorelease];
  } else {
    viewController = [[[AccountViewController alloc] initWithNibName:@"AccountViewController_iPad" bundle:nil] autorelease];
  }

  viewController.delegate = self;
  UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:viewController];
  [self presentModalViewController:navController animated:YES];
  [navController release];
}

- (void)showAlertMessage:(NSString*)message {
  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" 
                                                  message:message 
                                                 delegate:self
                                        cancelButtonTitle:@"OK"
                                        otherButtonTitles:nil];
  [alert show];
  [alert release];  
}

- (void)getHistoryForSelectedDate {

  float minTime = [[self.currentDate dateAtMidnight] timeIntervalSince1970] * 1000.0;
  float maxTime = [[[self.currentDate dateByAddingDays:1] dateAtMidnight] timeIntervalSince1970] * 1000.0;
  
  NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.googleapis.com/latitude/v1/location?granularity=best&max-results=1000&min-time=%.0f&max-time=%.0f", minTime, maxTime]];
  NSLog(@"URL: %@", url);
  
  [SVProgressHUD showInView:self.view withMaskType:SVProgressHUDMaskTypeGradient];
  
  NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
  [self.auth authorizeRequest:request
       completionHandler:^(NSError *error) {
         if (error) {
            
           [SVProgressHUD dismiss];
           [self showAlertMessage:[error localizedDescription]];
         } else {
           
           GTMHTTPFetcher* fetcher = [GTMHTTPFetcher fetcherWithRequest:request];

           [fetcher beginFetchWithCompletionHandler:^(NSData *retrievedData, NSError *fetchError) {
                          
             if (fetchError != nil) {
               [SVProgressHUD dismiss];
               [self showAlertMessage:[fetchError localizedDescription]];
             } else {
               
               NSString *output = [[[NSString alloc] initWithData:retrievedData
                                               encoding:NSUTF8StringEncoding] autorelease];    
               
               SBJSON *parser = [[SBJSON alloc] init];
               NSDictionary *objects = [parser objectWithString:output error:nil];
               [parser release];

               self.locations = [NSMutableArray array];
               
               for (NSDictionary *loc in [[objects objectForKey:@"data"] objectForKey:@"items"]) {
                 [self.locations addObject:[Location locationFromJSONDictionary:loc]];
               }
               
               self.locations = locations;
              
               [self updateMap];
               
             }
           }];
           
         }
         
       }];
}

- (void)updateMap {
  
  if ([self.locations count] == 0) {
    [SVProgressHUD dismissWithError:@"You have no location history for this day"];
  }
  else {
    [SVProgressHUD dismiss];
  }
  
  [self.mapView removeAnnotations:self.mapView.annotations];
  [self.mapView removeOverlays:self.mapView.overlays];
  
  [self.mapView addAnnotations:self.locations];
  [self addLines];
  [self zoomToFitAnnotations];
}

- (void)addLines {
  
  self.crumbs = nil;
  self.crumbView = nil;
  
  if ([self.locations count] == 0) {
    return;
  }
  
  Location *firstLocation = [self.locations objectAtIndex:0];
  self.crumbs = [[CrumbPath alloc] initWithCenterCoordinate:firstLocation.coordinate];
  [self.crumbs release];
  [self.mapView addOverlay:self.crumbs];
  
  
  for (Location *location in self.locations) {
    MKMapRect updateRect = [self.crumbs addCoordinate:location.coordinate];
    
    if (!MKMapRectIsNull(updateRect))
    {
      // There is a non null update rect.
      // Compute the currently visible map zoom scale
      MKZoomScale currentZoomScale = (CGFloat)(self.mapView.bounds.size.width / self.mapView.visibleMapRect.size.width);
      // Find out the line width at this zoom scale and outset the updateRect by that amount
      CGFloat lineWidth = MKRoadWidthAtZoomScale(currentZoomScale);
      updateRect = MKMapRectInset(updateRect, -lineWidth, -lineWidth);
      // Ask the overlay view to update just the changed area.
      [self.crumbView setNeedsDisplayInMapRect:updateRect];
    }
  }  
  
}

- (void)zoomToFitAnnotations {
  if ([self.locations count] == 0) {
    return;
  }
  
  int i = 0;
  MKMapPoint points[[self.mapView.annotations count]];
  
  //build array of annotation points
  for (id<MKAnnotation> annotation in [self.mapView annotations])
    points[i++] = MKMapPointForCoordinate(annotation.coordinate);
  
  MKPolygon *poly = [MKPolygon polygonWithPoints:points count:i];
  
  [self.mapView setRegion:MKCoordinateRegionForMapRect([poly boundingMapRect]) animated:YES]; 
}

#pragma mark - MKMapViewDelegate

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay {
  if ([overlay isKindOfClass:[MKCircle class]]) {
    MKCircleView *circleView = [[[MKCircleView alloc] initWithCircle:overlay] autorelease];
    circleView.lineWidth = 1.0;
    circleView.strokeColor = [UIColor blueColor];
    circleView.fillColor = [UIColor colorWithRed:0.0 green:0.0 blue:1.0 alpha:0.2];

    return circleView;
  }
  else if ([overlay isKindOfClass:[CrumbPath class]]) {
    if (!self.crumbView) {
      self.crumbView = [[CrumbPathView alloc] initWithOverlay:overlay];
      [self.crumbView release];
    }
    return crumbView;
  }
  
  return nil;
}

- (MKAnnotationView *)mapView:(MKMapView *)theMapView viewForAnnotation:(id <MKAnnotation>)annotation {
  // if it's the user location, just return nil.
  if ([annotation isKindOfClass:[MKUserLocation class]])
    return nil;
  
  // try to dequeue an existing pin view first
  static NSString* annotationIdentifier = @"LocationAnnotationIdentifier";
  RedMeasleAnnotationView* pinView = (RedMeasleAnnotationView *)
  [theMapView dequeueReusableAnnotationViewWithIdentifier:annotationIdentifier];
  if (!pinView) {
    pinView = [[[RedMeasleAnnotationView alloc]
                initWithAnnotation:annotation reuseIdentifier:annotationIdentifier] autorelease];
    pinView.canShowCallout = YES;  
    pinView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];

  }
  else  {
    pinView.annotation = annotation;
  }
  return (MKPinAnnotationView *)pinView;

}

- (void)mapView:(MKMapView *)theMapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
  
  PlacesViewController *placesViewController = [[PlacesViewController alloc] init];
  placesViewController.auth = self.auth;
  placesViewController.location = (Location*)view.annotation;
  
  [self.navigationController pushViewController:placesViewController animated:YES];
  [placesViewController release];
}

- (void)mapView:(MKMapView *)theMapView didSelectAnnotationView:(MKAnnotationView *)view {
  self.accuracyCircle = [MKCircle circleWithCenterCoordinate:view.annotation.coordinate radius:[(Location*)view.annotation accuracy]];
  [theMapView addOverlay:self.accuracyCircle];
}

- (void)mapView:(MKMapView *)theMapView didDeselectAnnotationView:(MKAnnotationView *)view {
  [theMapView removeOverlay:self.accuracyCircle];
}

#pragma mark - DateNavigationViewDelegate

- (void)previousDayButtonTapped {
  [self changeCurrentDateByNumberOfDays:-1];

}

- (void)nextDayButtonTapped {
  [self changeCurrentDateByNumberOfDays:1];
}

- (void)dateSelectionButtonTapped:(UIBarButtonItem *)sender {
  DateSelectionViewController *dateViewController = [[DateSelectionViewController alloc] init];
  dateViewController.delegate = self;
  
  if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
    self.dateSelectionPopoverController = [[UIPopoverController alloc] initWithContentViewController:dateViewController];
    [self.dateSelectionPopoverController release];
    self.dateSelectionPopoverController.popoverContentSize = CGSizeMake(320., 310.);
    self.dateSelectionPopoverController.delegate = self;
    
    [self.dateSelectionPopoverController presentPopoverFromRect:self.dateNavigationView.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];

  }
  else {
  
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:dateViewController];

    [self presentModalViewController:navController animated:YES];
    [navController release];
  }
  
  [dateViewController release];
}

#pragma mark - DateSelectionViewControllerDelegate

- (void)didSelectDate:(NSDate*)date {
  self.currentDate = date;
  [self getHistoryForSelectedDate];
  [self updateDateDisplay];
  
  if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
    [self.dateSelectionPopoverController dismissPopoverAnimated:YES];
  }
  else {
    [self dismissModalViewControllerAnimated:YES];  
  }
}

- (void)didCancelDateSelection {
  [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - UIPopoverControllerDelegate

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
  self.dateSelectionPopoverController = nil;
}

#pragma mark - Helpers

- (void)changeCurrentDateByNumberOfDays:(NSInteger)days {

  self.currentDate = [self.currentDate dateByAddingDays:days];
  
  [self getHistoryForSelectedDate];
  [self updateDateDisplay];
}

- (void)updateDateDisplay {
  NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
  [dateFormat setDateFormat:@"MMMM d"];
  NSString *dateString = [dateFormat stringFromDate:self.currentDate];  
  [dateFormat release];
  
  self.dateNavigationView.dateSelectionItem.title = dateString;
}

- (void)changeMapType:(UISegmentedControl*)control {
  self.mapView.mapType = control.selectedSegmentIndex;
}

@end
