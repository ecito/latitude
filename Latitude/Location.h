//
//  Location.h
//  Latitude
//
//  Created by Andre Navarro on 10/30/11.
//  Copyright (c) 2011 Disruptive Media Labs, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface Location : NSObject <MKAnnotation>

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, assign) double timestamp;
@property (nonatomic, assign) double accuracy;
@property (nonatomic, readonly) CLLocation *location;

+ (Location*)locationFromJSONDictionary:(NSDictionary*)dictionary;

@end
