//
//  RedMeasleAnnotationView.m
//  Latitude
//
//  Created by Andre Navarro on 11/1/11.
//  Copyright (c) 2011 Disruptive Media Labs, LLC. All rights reserved.
//

#import "RedMeasleAnnotationView.h"

@implementation RedMeasleAnnotationView
  
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
      self.image = [UIImage imageNamed:@"red_measle.png"];
    }
    return self;
}

@end
