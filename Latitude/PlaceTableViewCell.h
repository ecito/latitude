//
//  PlaceTableViewCell.h
//  Latitude
//
//  Created by Andre Navarro on 10/31/11.
//  Copyright (c) 2011 Disruptive Media Labs, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Place;

@interface PlaceTableViewCell : UITableViewCell

@property (nonatomic, retain) Place *place;

@end
