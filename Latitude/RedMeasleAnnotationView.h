//
//  RedMeasleAnnotationView.h
//  Latitude
//
//  Created by Andre Navarro on 11/1/11.
//  Copyright (c) 2011 Disruptive Media Labs, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface RedMeasleAnnotationView : MKAnnotationView

@end
