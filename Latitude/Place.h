//
//  Place.h
//  Latitude
//
//  Created by Andre Navarro on 11/1/11.
//  Copyright (c) 2011 Disruptive Media Labs, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface Place : NSObject <MKAnnotation>

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *icon;
@property (nonatomic, copy) NSString *vicinity;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, readonly) CLLocation *location;

+ (Place*)placeFromJSONDictionary:(NSDictionary*)dictionary;

@end