//
//  PlaceTableViewCell.m
//  Latitude
//
//  Created by Andre Navarro on 10/31/11.
//  Copyright (c) 2011 Disruptive Media Labs, LLC. All rights reserved.
//

#import "PlaceTableViewCell.h"

#import "Place.h"

#import "UIImageView+AFNetworking.h"

@implementation PlaceTableViewCell
@synthesize place;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) {
        return nil; 
    }
    
    self.textLabel.textColor = [UIColor darkGrayColor];
    self.textLabel.numberOfLines = 2;
    self.textLabel.backgroundColor = self.backgroundColor;
    
    self.detailTextLabel.textColor = [UIColor grayColor];
    self.detailTextLabel.backgroundColor = self.backgroundColor;

    self.imageView.backgroundColor = self.backgroundColor;
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    self.selectionStyle = UITableViewCellSelectionStyleGray;
    
    return self;
}

- (void)dealloc {

  self.place = nil;
  [super dealloc];
}

- (void)setPlace:(Place *)aPlace {
  [self willChangeValueForKey:@"place"];
  place = aPlace;
  [self didChangeValueForKey:@"place"];

  [self.imageView setImageWithURL:[NSURL URLWithString:self.place.icon] placeholderImage:[UIImage imageNamed:@"generic_business-71.png"]];
     
  self.textLabel.text = place.name;
}

#pragma mark - UITableViewCell

- (void)prepareForReuse {
  [super prepareForReuse];
  [self.imageView cancelImageRequestOperation];
  self.textLabel.text = nil;
  self.detailTextLabel.text = nil;
}

#pragma mark - UIView

- (void)layoutSubviews {
  [super layoutSubviews];
  CGRect imageViewFrame = self.imageView.frame;
  CGRect textLabelFrame = self.textLabel.frame;
  CGRect detailTextLabelFrame = self.detailTextLabel.frame;

  imageViewFrame.origin = CGPointMake(10.0f, 10.0f);
  imageViewFrame.size = CGSizeMake(50.0f, 50.0f);
  textLabelFrame.origin.x = imageViewFrame.size.width + 25.0f;
  detailTextLabelFrame.origin.x = textLabelFrame.origin.x;
  textLabelFrame.size.width = 240.0f;
  detailTextLabelFrame.size.width = textLabelFrame.size.width;

  self.textLabel.frame = textLabelFrame;
  self.detailTextLabel.frame = detailTextLabelFrame;
  self.imageView.frame = imageViewFrame;
}

@end
