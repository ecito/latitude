//
//  DateSelectionViewController.h
//  Latitude
//
//  Created by Andre Navarro on 10/31/11.
//  Copyright (c) 2011 Disruptive Media Labs, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TapkuLibrary/TapkuLibrary.h>

@protocol DateSelectionViewControllerDelegate <NSObject>

- (void)didSelectDate:(NSDate*)date;
- (void)didCancelDateSelection;

@property (nonatomic, readonly) NSDate *currentDate;

@end

@interface DateSelectionViewController : UIViewController <TKCalendarMonthViewDataSource, TKCalendarMonthViewDelegate>

@property (nonatomic, assign) id<DateSelectionViewControllerDelegate> delegate;
@end
