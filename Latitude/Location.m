//
//  Location.m
//  Latitude
//
//  Created by Andre Navarro on 10/30/11.
//  Copyright (c) 2011 Disruptive Media Labs, LLC. All rights reserved.
//

#import "Location.h"
#import "NSDate+HumanInterval.h"

@implementation Location
@synthesize coordinate;
@synthesize timestamp;
@synthesize accuracy;

+ (Location*)locationFromJSONDictionary:(NSDictionary*)dictionary {

  Location *location = [[[Location alloc] init] autorelease];

  location.timestamp = [[dictionary objectForKey:@"timestampMs"] doubleValue];
  location.accuracy = [[dictionary objectForKey:@"accuracy"] doubleValue];

  double latitude = [[dictionary objectForKey:@"latitude"] doubleValue];
  double longitude = [[dictionary objectForKey:@"longitude"] doubleValue];
    
  CLLocationCoordinate2D coordinate = {latitude, longitude};
  location.coordinate = coordinate;
  
  return location;
}

- (NSString*)title {
  return [NSString stringWithFormat:@"Here %@ ago", 
          [[NSDate dateWithTimeIntervalSince1970:self.timestamp / 1000.0] humanIntervalSinceNow]];
}

- (NSString*)subtitle {
  return [[NSDate dateWithTimeIntervalSince1970:self.timestamp / 1000.0] mediumDateShortTimeString];
}

- (CLLocation*)location {
  return [[[CLLocation alloc] initWithLatitude:self.coordinate.latitude longitude:self.coordinate.longitude] autorelease];
}

@end
