//
//  ViewController.m
//  Latitude
//
//  Created by Andre Navarro on 10/30/11.
//  Copyright (c) 2011 Disruptive Media Labs, LLC. All rights reserved.
//

#import "AccountViewController.h"
#import "GTMOAuth2ViewControllerTouch.h"
#import "SBJSON.h"
#import "Location.h"
#import "MapViewController.h"
#import "Constants.h"

@implementation AccountViewController
@synthesize signInOutButton;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    self.modalTransitionStyle = UIModalTransitionStyleCoverVertical;

    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancel)] autorelease];
  }
  return self;
}

- (void)cancel {
  if (self.delegate && [self.delegate respondsToSelector:@selector(didCancelAccountView)]) {
    [self.delegate didCancelAccountView];
  }
}

#pragma mark - View lifecycle

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
 
  GTMOAuth2Authentication *auth = [GTMOAuth2ViewControllerTouch authForGoogleFromKeychainForName:kKeychainItemName
                                                                                        clientID:kClientID
                                                                                    clientSecret:kClientSecret];
  
  if ([auth canAuthorize]) {
    [self.signInOutButton setTitle:@"Sign Out" forState:UIControlStateNormal];
  }
  else {
    self.navigationItem.leftBarButtonItem = nil;
  }
}

- (IBAction)signInOut:(id)sender {

  GTMOAuth2Authentication *auth = [GTMOAuth2ViewControllerTouch authForGoogleFromKeychainForName:kKeychainItemName
                                                                                        clientID:kClientID
                                                                                    clientSecret:kClientSecret];

  if ([auth canAuthorize]) {
    [GTMOAuth2ViewControllerTouch removeAuthFromKeychainForName:kKeychainItemName];
    
    [GTMOAuth2ViewControllerTouch revokeTokenForGoogleAuthentication:auth];
    
    self.navigationItem.leftBarButtonItem = nil;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSignOut)]) {
      [self.delegate didSignOut];
    }
    
    [self.signInOutButton setTitle:@"Sign In" forState:UIControlStateNormal];
    
  } else {
    
    GTMOAuth2ViewControllerTouch *viewController;
    viewController = [[[GTMOAuth2ViewControllerTouch alloc] initWithScope:kScope
                                                                 clientID:kClientID
                                                             clientSecret:kClientSecret
                                                         keychainItemName:kKeychainItemName
                                                                 delegate:self
                                                         finishedSelector:@selector(viewController:finishedWithAuth:error:)] autorelease];
    
    [[self navigationController] pushViewController:viewController
                                           animated:YES];  
  }
}

- (void)viewController:(GTMOAuth2ViewControllerTouch *)viewController
      finishedWithAuth:(GTMOAuth2Authentication *)auth
                 error:(NSError *)error {
  if (error != nil) {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" 
                                                    message:@"Could not sign in to Google" 
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];    
  } else {

    if (self.delegate && [self.delegate respondsToSelector:@selector(didSignInWithAuth:)]) {
      [self.delegate didSignInWithAuth:auth];
    }
  }
}


- (void)dealloc {
  [signInOutButton release];
  [super dealloc];
}
- (void)viewDidUnload {
  [self setSignInOutButton:nil];
  [super viewDidUnload];
}
@end
