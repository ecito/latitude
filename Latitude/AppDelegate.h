//
//  AppDelegate.h
//  Latitude
//
//  Created by Andre Navarro on 10/30/11.
//  Copyright (c) 2011 Disruptive Media Labs, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AccountViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) AccountViewController *viewController;

@end
