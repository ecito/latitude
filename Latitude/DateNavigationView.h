//
//  DateNavigationView.h
//  Latitude
//
//  Created by Andre Navarro on 10/31/11.
//  Copyright (c) 2011 Disruptive Media Labs, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DateNavigationViewDelegate <NSObject>

- (void)dateSelectionButtonTapped:(UIBarButtonItem*)sender;

- (void)previousDayButtonTapped;
- (void)nextDayButtonTapped;
@end

@interface DateNavigationView : UIToolbar

@property (nonatomic, assign) id<DateNavigationViewDelegate> delegate;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *dateSelectionItem;

- (IBAction)dateSelectionButtonTapped:(id)sender;

- (IBAction)previousDayButtonTapped:(id)sender;
- (IBAction)nextDayButtonTapped:(id)sender;

@end
