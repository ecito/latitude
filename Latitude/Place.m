//
//  Place.m
//  Latitude
//
//  Created by Andre Navarro on 11/1/11.
//  Copyright (c) 2011 Disruptive Media Labs, LLC. All rights reserved.
//

#import "Place.h"

@implementation Place
@synthesize name;
@synthesize icon;
@synthesize coordinate;
@synthesize vicinity;

- (void)dealloc {
  
  self.name = nil;
  self.icon = nil;
  self.vicinity = nil;
  
  [super dealloc];
}

+ (Place*)placeFromJSONDictionary:(NSDictionary*)dictionary {
  
  Place *place = [[[Place alloc] init] autorelease];
  
  place.name = [dictionary objectForKey:@"name"];
  place.icon = [dictionary objectForKey:@"icon"];
  place.vicinity = [dictionary objectForKey:@"vicinity"];
  
  double latitude = [[[[dictionary objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lat"] doubleValue];
  double longitude = [[[[dictionary objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lng"] doubleValue];
  
  CLLocationCoordinate2D coordinate = {latitude, longitude};
  place.coordinate = coordinate;
  
  return place;
}

- (NSString*)title {
  return self.name;
}

- (CLLocation*)location {
  return [[[CLLocation alloc] initWithLatitude:self.coordinate.latitude longitude:self.coordinate.longitude] autorelease];
}

@end
